//13-08-2020

const express = require('express')
const bodyParser = require('body-parser')
const connectDB = require('./config/db')
var cors = require('cors')
const app = express()
const port = 3001

//Middleware
app.use(cors())
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

//Connect DB

connectDB()

//Routes

//app.use('/', require('./routes/hello'))
//app.use('/form', require('./routes/api/form'))
app.use('/user', require('./routes/api/user'))


app.listen(port, () => { console.log(`APP operativa`) })