import { clientHttp } from '../config/config.js'

const createUser = (data) => clientHttp.post(`/user`, data)

const ListUser = () => clientHttp.get(`/user`)

const DeleteUser = (email) => clientHttp.delete(`/user/${email}`)

export {
    createUser,
    ListUser,
    DeleteUser
}
