import UserList from './components/user/lista'
import UserCreate from './components/user/create'
import Login from './components/login/login'


const routes = {
    'Lista': {
        component: UserList
    },
    'Create': {
        component: UserCreate
    },
    'Login': {
        component: Login       
    },
}

export default routes;
