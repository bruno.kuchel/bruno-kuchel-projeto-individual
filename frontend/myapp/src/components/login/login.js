import React, { useState } from 'react'
import './login.css'

const Login = (props) => {
    const [auth, setAuth] = useState({})

    const handleChange = (event) => {
        setAuth({
            ...auth,
            [event.target.name] : event.target.value
        })
        return;
    }

    const formIsValid = () => {
        return auth.email && auth.password
    }

    const submitForm = (event) => {
        if(formIsValid()){
            props.mudaPagina('Lista')
        }
         }

return (
    <section className="section-login">
        <div className="title">
            <h1>Acesso ao sistema</h1>
        </div>
        <div className="login">
        <div className="form-login">                  
                    <div className="row">
                        <div className="etiqueta">
                            <label htmlFor="email">Identificação</label>
                        </div>
                        <div className="form-input">
                            <input className="campo" type="email" name="email" onChange={handleChange} value={auth.email || ""} placeholder="Por favor, digite seu e-mail"/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="etiqueta">
                            <label htmlFor="password">Senha</label>
                        </div>
                        <div className="form-input">
                            <input className="campo" type="password" name="password" onChange={handleChange} value={auth.password || ""} placeholder="Por favor, digite sua senha"/>
                        </div>
                    </div>
                    <div className="row">    
                        <button disabled={!formIsValid()} className="btn cadastrar" onClick={() => submitForm()}>Entrar</button>    
                    </div>                                   
        </div>
        </div>
    </section>
)
}

export default Login;