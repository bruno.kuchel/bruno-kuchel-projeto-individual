import React from 'react'
import './header.css'
import logo from '../../../assets/img/asgard-health4.png'

const Header = () => (
    <header>
        <img src={logo} alt="Asgard Health"/>
    </header>       
)
export default Header
