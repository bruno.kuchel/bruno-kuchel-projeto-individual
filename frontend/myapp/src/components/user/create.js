import React, { useState } from 'react'
import { createUser } from '../../services/user'
import './user.css'

const UserCreate = (props) => {
    const [form, setForm] = useState({
        is_active: true,
        is_admin: false

    })

    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name] : event.target.value            
        })
        return;
    }

    const formIsValid = () => {
        let radioButtonIsValid = false
        let radiosButtons = document.getElementsByName('sexo')
    
        radiosButtons.forEach((radioButton) => {
            if(radioButton.checked){
                radioButtonIsValid = true
            }
        })
        return (
            radioButtonIsValid &&
          form.nascimento &&
          form.nome &&
          form.sobrenome &&
          form.rg &&
          form.email &&
          form.telefone
        )
      }

    
    

    const submitForm = async (event) => {
        try {
            await createUser(form)
            alert('O formulário foi enviado com sucesso')
            setForm({
                is_active: true,
                is_admin: false
            })
            props.mudaPagina('Lista')
        } catch (error) {
            console.log('deu ruim')
            
        }
    }
    
    const cancel = (event) => {    
        props.mudaPagina('Lista')
    }

    return (
        <section>
            <div className="title">
                <h1>Cadastro de pacientes</h1>
            </div>
            <div className="cadastroPaciente">                    
                        <div className="row">                            
                            <div className="radios">
                                <div className="etiqueta">Sexo</div>
                                <div className="radio">
                                    <input className="option" type="radio" id="masculino" name="sexo" onChange={handleChange} value="Masculino"/*{form.masculino || ""}*//>
                                    <label htmlFor="masculino">Masculino</label>
                                </div>
                                <div className="radio">
                                    <input className="option" type="radio" id="feminino" name="sexo" onChange={handleChange} value="Feminino"/*{form.feminino || ""}*//>
                                    <label htmlFor="feminino">Feminino</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="etiqueta">
                                <label htmlFor="nascimento">Nascimento:</label>
                            </div>
                            <div className="form-input">
                                <input className="campo" type="date" name="nascimento" onChange={handleChange} value={form.nascimento || ""} placeholder="Nascimento"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="etiqueta">
                                <label htmlFor="nome">Nome:</label>
                            </div>
                            <div className="form-input">
                                <input className="campo" type="text" name="nome" label="Nome:" onChange={handleChange} value={form.nome || ""} placeholder="Insira o nome do paciente"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="etiqueta">
                                <label htmlFor="sobrenome">Sobrenome:</label>
                            </div>
                            <div className="form-input">
                                <input className="campo" type="text" name="sobrenome" onChange={handleChange} value={form.sobrenome || ""} placeholder="Insira o sobrenome do paciente"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="etiqueta">
                                <label htmlFor="rg">RG:</label>
                            </div>
                            <div className="form-input">
                                <input className="campo" type="number" min="0" name="rg" onChange={handleChange} value={form.rg || ""} placeholder="Insira o RG do paciente"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="etiqueta">
                                <label htmlFor="email">E-mail:</label>
                            </div>
                            <div className="form-input">
                                <input className="campo" type="email" name="email" onChange={handleChange} value={form.email || ""} placeholder="Insira o e-mail do paciente"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="etiqueta">
                                <label htmlFor="telefone">Telefone:</label>
                            </div>
                            <div className="form-input">
                                <input className="campo" type="tel" name="telefone" onChange={handleChange} value={form.telefone || ""} placeholder="Insira o telefone do paciente"/>
                            </div>
                        </div>
                        <div className="row">    
                            <button disabled={!formIsValid()} className="btn cadastrar" onClick={submitForm}>CADASTRAR</button>    
                        </div>
                        <div className="row">    
                            <button className="btn cadastrar" onClick={cancel}>CANCELAR</button>    
                        </div>                                   
            </div>
        </section>
)
}
export default UserCreate
