import React, {useEffect, useState} from 'react'
import {ListUser, DeleteUser} from '../../services/user'
import Loading from '../loading/loading'
import './user.css'
import iconEdit from '../../assets/img/user-edit.svg'
import iconDelete from '../../assets/img/user-delete.svg'

const UserList = (props) => {
    const [users, setUsers] = useState([])

    const getList = async () => {
        try {
            const users = await ListUser()
            setUsers(users.data)
        } catch (error) {
            console.log('error', error)
        }
    }


const editUser = (user) => console.log('edit', user)

const deleteUser = async ({email, nome, sobrenome}) => {
    try {
        if (window.confirm(`Deseja excluir o ${nome} ${sobrenome} ?`)){
            await DeleteUser(email)
            getList()
        }
    } catch (error) {
        console.log(error)
    }
}

const verifyIsEmpty = users.length === 0

const montarLista = () => {
    const row = users.map((user, index) =>(
        <tr key={index}>
            <td>{user.sexo}</td>
            <td>{user.nascimento}</td>
            <td>{user.nome}</td>
            <td>{user.sobrenome}</td>
            <td>{user.rg}</td>
            <td>{user.email}</td>
            <td>{user.telefone}</td>
            <td> 
                <img src ={iconEdit} className ="iconEdit" alt="Editar" onClick={() => editUser(user)}/>        
                <img src ={iconDelete} className ="iconDelete" alt="Excluir" onClick={() => deleteUser(user)}/>   
            </td>
        </tr>
    ))

    return !verifyIsEmpty ? (
        <table>
        <thead>
            <tr>
                <th>Sexo</th>
                <th>Nascimento</th>
                <th>Nome</th>
                <th>Sobrenome</th>
                <th>RG</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Ação</th>
            </tr>
        </thead>
        <tbody>
            {row}
        </tbody>
    </table>       
    ) : ""

}

useEffect(() => {
    getList()
}, [])

const addPaciente = (event) => {    
        props.mudaPagina('Create')
    }
     

return (
    <section>
            <div className="title">
                <h1>Lista de pacientes</h1>
            </div>
            <div className="listaPacientes">                
                <nav>
                    <div className="action">
                        <button className="btn adicionar" name="adicionar" onClick={() => addPaciente()}>ADICIONAR PACIENTE</button>
                    </div>
                </nav>
               <Loading show={verifyIsEmpty}/>
               {montarLista()}
            </div>
        </section>
)
}

export default UserList