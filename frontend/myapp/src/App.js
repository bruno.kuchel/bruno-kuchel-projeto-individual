import React, {useState} from 'react';
import './App.css';
import Layout from './components/layout'
import routes from './routes'

function App() {
  const [page, setPage] = useState('Login')
  const rotaSelecionada = routes[page]
  const ComponenteSelecionado = rotaSelecionada.component

  return (
    <React.Fragment>
    <Layout>
      <ComponenteSelecionado mudaPagina={setPage}/>
    </Layout>
    </React.Fragment>
  )

}

export default App;
